//
//  CountryPickerController.h
//  CheckPhoneNumber
//
//  Created by NhatLe on 6/11/14.
//  Copyright (c) 2014 NhatLe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "CountryPicker.h"

@protocol CountryPickerViewDelegate <NSObject>

- (void) countryPickerSave:(NSString*) country andCode:(NSString*) code;

@end
#import "ViewController.h"
@interface CountryPickerController : UIViewController<CountryPickerDelegate, UIPickerViewDelegate>{
//    id<CountryPickerViewDelegate>__unsafe_unretained delegate;
}
@property (nonatomic, strong) IBOutlet UILabel *lblCountry;
@property (nonatomic, strong) IBOutlet UILabel *lblCode;
@property (nonatomic, strong) IBOutlet CountryPicker *countryPicker;
@property (nonatomic, strong) id<CountryPickerViewDelegate> delegate;
@property (nonatomic, strong) NSString *currentCode;
@property (nonatomic, strong) NSString *currentName;
-(IBAction)choseAction:(id)sender;
@end
