//
//  AppDelegate.h
//  CheckPhoneNumber
//
//  Created by NhatLe on 6/10/14.
//  Copyright (c) 2014 NhatLe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
