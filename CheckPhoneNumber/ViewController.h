//
//  ViewController.h
//  CheckPhoneNumber
//
//  Created by NhatLe on 6/10/14.
//  Copyright (c) 2014 NhatLe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryPickerController.h"

@interface ViewController : UIViewController<UITextFieldDelegate>{
    IBOutlet UITextField *txtPhoneNumber;
    IBOutlet UITextField *txtCountryCode;
    UIAlertView *alert;
    NSString *countryName;
}
@property (nonatomic, strong) IBOutlet UITextField *txtPhoneNumber;
@property (nonatomic, strong) IBOutlet UITextField *txtCountryCode;
-(IBAction)validatePhoneNumber:(id)sender;
@end
