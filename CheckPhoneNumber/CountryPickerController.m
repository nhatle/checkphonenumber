//
//  CountryPickerController.m
//  CheckPhoneNumber
//
//  Created by NhatLe on 6/11/14.
//  Copyright (c) 2014 NhatLe. All rights reserved.
//

#import "CountryPickerController.h"

@interface CountryPickerController ()

@end

@implementation CountryPickerController
@synthesize lblCountry,lblCode;
@synthesize delegate;
@synthesize countryPicker;
@synthesize currentCode, currentName;

- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    if ([name length]==0 || [code length]==0) {
        lblCountry.text = @"Afghanistan";
        lblCode.text = @"AF";
    }else{
        lblCountry.text = name;
        lblCode.text = code;
    }
    
}
-(IBAction)choseAction:(id)sender{
    if ([lblCountry.text length]==0 || [lblCode.text length]==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Please chose your country" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        [self.delegate countryPickerSave:lblCountry.text andCode:lblCode.text];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [countryPicker setSelectedCountryName:currentName animated:YES];
    [self countryPicker:countryPicker didSelectCountryWithName:currentName code:currentCode];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
