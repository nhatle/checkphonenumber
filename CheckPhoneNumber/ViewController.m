//
//  ViewController.m
//  CheckPhoneNumber
//
//  Created by NhatLe on 6/10/14.
//  Copyright (c) 2014 NhatLe. All rights reserved.
//

#import "ViewController.h"
#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumber.h"
#import "CountryPickerController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize txtCountryCode,txtPhoneNumber;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disMissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
}
-(void)disMissKeyboard{
    [txtPhoneNumber resignFirstResponder];
}
-(void)checkPhoneNumber:(NSString *)number andRegion:(NSString*)region{
    NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil sharedInstance];
    NSError *error = nil;
    NBPhoneNumber *myNumber = [phoneUtil parseAndKeepRawInput:txtPhoneNumber.text defaultRegion:txtCountryCode.text error:&error];
    if (error == nil) {
        
        if ([phoneUtil isValidNumber:myNumber]) {
            alert = [[UIAlertView alloc] initWithTitle:@"Validation" message:[NSString stringWithFormat:@"%@ is a valid phone number at %@.", number, countryName] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else{
            alert = [[UIAlertView alloc] initWithTitle:@"Validation" message:[NSString stringWithFormat:@"%@ is NOT a valid phone number at %@. Please check again.", number, countryName] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    } else {
        NSLog(@"Error : %@", [error localizedDescription]);
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [errorAlert show];
    }
    
}

-(IBAction)validatePhoneNumber:(id)sender{
    [txtPhoneNumber resignFirstResponder];
    [self checkPhoneNumber:txtPhoneNumber.text andRegion:txtCountryCode.text];
}
#pragma mark -
#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.txtCountryCode) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            CountryPickerController *pickerController = [[CountryPickerController alloc] initWithNibName:@"CountryPickerController_iPad" bundle:nil];
            pickerController.delegate = self;
            pickerController.currentCode = self.txtCountryCode.text;
            pickerController.currentName = countryName;
            [self presentViewController:pickerController animated:YES completion:nil];
        }
        else{
            CountryPickerController *countryPicker = [[CountryPickerController alloc] init];
            countryPicker.delegate = self;
            countryPicker.currentCode = self.txtCountryCode.text;
            countryPicker.currentName = countryName;
            [self presentViewController:countryPicker animated:YES completion:nil];
        }
        return NO;
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
- (void) countryPickerSave:(NSString*) country andCode:(NSString *)code{
    countryName = country;
    self.txtCountryCode.text = code;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
